﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CPRG_214_A_Lab3_Prince_Nimoh.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="styles/main.css" rel="stylesheet" />
</head>
<body>
    <div class="container main">
         <form id="form1" runat="server">
             <div class="jumbotron text-center">
                 <h2>Tech Support Dashboard</h2>
             </div>
             <div class="form-row">
            <asp:DropDownList ID="ddlTechs" runat="server" AutoPostBack="True" class="dropdown" DataSourceID="ObjectDataSource1" DataTextField="Name" DataValueField="TechID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetTechnicians" TypeName="CPRG_214_A_Lab3_Prince_Nimoh.App_Code.TechnicianDB"></asp:ObjectDataSource>
            </div>
             <br />
             <br />
             <div class="form-row">
                 <asp:GridView ID="gvIncidents" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource2" class="form-group col-md-12" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                    <Columns>
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName"  />
                        <asp:BoundField DataField="ProductCode" HeaderText="Product Code" SortExpression="ProductCode" />
                        <asp:BoundField DataField="TechID" HeaderText="Technician ID" SortExpression="TechID" />
                        <asp:BoundField DataField="DateOpened" HeaderText="Date Opened" SortExpression="DateOpened" />
                        <asp:BoundField DataField="DateClosed" HeaderText="Date Closed" SortExpression="DateClosed" />
                        <asp:BoundField DataField="Title" HeaderText="Incident Title" SortExpression="Title" />
                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Text="No open incidents available for this technician." class="btn btn-light"></asp:Label>
                    </EmptyDataTemplate>
                     <FooterStyle BackColor="White" ForeColor="#000066" />
                     <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                     <RowStyle ForeColor="#000066" />
                     <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#F1F1F1" />
                     <SortedAscendingHeaderStyle BackColor="#007DBB" />
                     <SortedDescendingCellStyle BackColor="#CAC9C9" />
                     <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetOpenIncidentsByTechID" TypeName="CPRG_214_A_Lab3_Prince_Nimoh.App_Code.IncidentDB">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlTechs" Name="techID" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        <div>
        </div>
    </form>
    </div>
    <script src="Scripts/jquery-3.0.0.js"></script>
    <script src="Scripts/popper.js"></script>
    <script src="Scripts/bootstrap.js"></script>
</body>
</html>
