﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab3_Prince_Nimoh.App_Code
{

    /**
    * Author: Prince Nimoh
    * Context: CPRG214-Lab3-Prince Nimoh
    * Student #: 000792122
    * Date: July 2018
    *Data access class for the technician class. 
    * */
    [DataObject(true)]
    public class TechnicianDB
    {
        /// <summary>
        /// Reads technician objects from the TechSupport DB
        /// </summary>
        /// <returns>returns a list of technicians</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<Technician> GetTechnicians()
        {
            List<Technician> technicians = new List<Technician>(); // make an empty list
            Technician technician; // reference to new state object
            // create connection
            SqlConnection connection = TechSupportDB.GetConnection();

            // create select command
            string selectString = "SELECT * from Technicians " +
                                  "order by Name";
            SqlCommand selectCommand = new SqlCommand(selectString, connection);
            try
            {
                // open connection
                connection.Open();
                // run the select command and process the results adding technicians to the list
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())// process next row
                {
                    technician = new Technician();
                    technician.TechID = Convert.ToInt32(reader["TechID"]);
                    technician.Name = reader["Name"].ToString();
                    technician.Phone = reader["Phone"].ToString();
                    technician.Email = reader["Email"].ToString();
                    technicians.Add(technician);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex; // throw it to the form to handle
            }
            finally
            {
                connection.Close();
            }
            return technicians;
        }
    }
}