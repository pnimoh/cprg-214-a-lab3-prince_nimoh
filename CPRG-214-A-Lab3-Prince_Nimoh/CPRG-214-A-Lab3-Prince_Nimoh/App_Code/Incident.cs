﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab3_Prince_Nimoh.App_Code
{
 /**
 * Author: Prince Nimoh
 * Context: CPRG214-Lab3-Prince Nimoh
 * Student #: 000792122
 * Date: July 2018
 *Incident Entity class
 * */
    public class Incident
    {
        public int IncidentID { get; set; } //Incident ID - primary key for incident table

        public string CustomerName { get; set; } //The name of the customer the incident is associated with

        public string ProductCode { get; set; } //The code for the product the incident is related to - foriegn key

        public int TechID { get; set; } //The ID for the technician assigned to the incident

        public DateTime DateOpened { get; set; } //The date the incident was opened

        public DateTime? DateClosed { get; set; } //The date the incident was closed

        public string Title { get; set; } //The title of the incident

        public string Description { get; set; } //The description of the incident
    }
}