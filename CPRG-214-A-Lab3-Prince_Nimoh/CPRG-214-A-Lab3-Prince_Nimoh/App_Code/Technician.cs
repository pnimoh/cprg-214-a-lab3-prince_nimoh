﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab3_Prince_Nimoh.App_Code
{
 /**
 * Author: Prince Nimoh
 * Context: CPRG214-Lab3-Prince Nimoh
 * Student #: 000792122
 * Date: July 2018
 *Technician entity class. 
 * */
    public class Technician
    {
        public int TechID { get; set; } //Technician's ID - primary key

        public string Name { get; set; } //Technician's name

        public string Email { get; set; } //Technician's email address

        public string Phone { get; set; } //Technician's Phone number
    }
}